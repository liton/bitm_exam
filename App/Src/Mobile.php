<?php
/**
 * Description of Mobile
 *`id`, `title`, `mobile_model`, `check`
 * @author Liton SEIP ID : 131649
 */

namespace App;

use PDO;

class Mobile
{
    public $id = '';
    public $title = "";
    public $mobile_model = "";
    public $check1 = '';
    public $data = '';
    public $user = 'root';
    public $pw = '';
    public $conn = '';
    
    


    public function __construct($data = false)
    {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=bitm_exam', $this->user, $this->pw);

        if (!empty($data['title'])) {
            $this->title = $data['title'];
        } else {
            $_SESSION['title'] = "Required Your Name";
        }

        if (!empty($data['mobile_model'])) {
            $this->mobile_model = $data['mobile_model'];
        } else {
            $_SESSION['mbl'] = "Required Mobile Model";
        }


        if (!empty($data['check1'])) {
            $this->check1 = $data['check1'];
        } else {
            $_SESSION['che'] = "Required check";
        }
        $this->id = @$data['id'];

        return $this;
    }

    public function store()
    {
        try {
            $query = "INSERT INTO mobile(title, mobile_model, check1,unique_id)
                        VALUES(:title, :m, :c,:u)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':title' => $this->title,
                ':m' => $this->mobile_model,
                ':c' => $this->check1,
                ':u'=> uniqid()
            ));
            $_SESSION['Message'] = 'Add Data Succesfully!';
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        try {

            $query = "SELECT * FROM `mobile` ";
            $result = $this->conn->query($query) or die('Query Error!');
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show()
    {
        $query = "SELECT * FROM `mobile` WHERE unique_id=" . "\"$this->id\"";
        $result = $this->conn->query($query) or die('Error query');
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
        return $this;
    }

    public function update()
    {
        $query = "UPDATE `bitm_exam`.`mobile` SET `title` = '$this->title', `mobile_model` = '$this->mobile_model',`check1`='$this->check1' WHERE `mobile`.`unique_id` =". "\"$this->id\"";
// var_dump($query) or die();
        $result = $this->conn->query($query) or die('Error query');
        if(isset($result)){
            $_SESSION['Message'] = "Data Update successfully ";
            header('location:index.php');
        }


    }

    public function delete()
    {
        $query = "DELETE FROM `bitm_exam`.`mobile` WHERE `mobile`.`unique_id` =" . "\"$this->id\"";
        $result = $this->conn->query($query) or die('Query Error!');
        if(isset($result)) {
            $_SESSION['Message'] = "Delete Information!";
            header('location:index.php');
        } else {

        }

    }
}












