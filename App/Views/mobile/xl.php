<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from a web Browser');


require_once '../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../vendor/autoload.php';

use App\Mobile;

$obj = new Mobile();
$allData = $obj->index();


$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator('Maarten Balliaue')
            ->setLastModifiedBy('Maarten Balliauw')
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test documnet for Office 2007 XLSX, generated using PHP class")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");

$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','SL')
        ->setCellValue('B1','ID')
        ->setCellValue('C1','Title')
        ->setCellValue('D1','Mobile Model')
    ->setCellValue('E1','Checked');
$counter = 2;
$serial = 0;
foreach ($allData as $data){
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter,$serial)
            ->setCellValue('B'.$counter,$data['id'])
            ->setCellValue('C'.$counter,$data['title'])
            ->setCellValue('D'.$counter,$data['mobile_model'])
        ->setCellValue('D'.$counter,$data['check1']);
    $counter++;
}
    $objPHPExcel->getActiveSheet()->setTitle('Mobile_model_list');
    
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('content-Disposition: attachment;filename="01simple.xls"');
    header('cache-control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Ffiday, 8 July 2016 GMT');
    header('Last-Modified: '.gmdate('D,d M Y H:i:s').'GMT');
    header('Pragma: public');
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
    
            

       
       